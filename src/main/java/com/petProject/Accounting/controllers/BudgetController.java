package com.petProject.Accounting.controllers;

import com.petProject.Accounting.entities.Budget;
import com.petProject.Accounting.service.BudgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Base64;

@Controller
public class BudgetController {
    @Autowired
    private BudgetService budgetService;
    @GetMapping("/createBudgets")
    public String showCreateBudgetsPage(Model model) {
        model.addAttribute("budgets", budgetService.getAllBudgets());
        return "createBudgets";
    }
    @PostMapping("/createBudget")
    public String createBudget(@RequestParam("budgetName") String budgetName) {
        Budget budget = new Budget();
        budget.setName(budgetName);
        budgetService.saveOrUpdateBudget(budget);
        return "redirect:/createBudgets";
    }
    @PostMapping("/updateBudgetName")
    public String updateBudgetName(
            @RequestParam("budgetId") Long budgetId,
            @RequestParam("updatedName") String updatedName
    ) {
        Budget existingBudget = budgetService.getBudgetById(budgetId);
        if (existingBudget != null) {
            existingBudget.setName(updatedName);
            budgetService.saveOrUpdateBudget(existingBudget);
        }
        return "redirect:/createBudgets";
    }

    @GetMapping("/deleteBudget/{budgetId}")
    public String deleteBudget(@PathVariable Long budgetId) {
        budgetService.deleteBudget(budgetId);

        return "redirect:/createBudgets";
    }
}
