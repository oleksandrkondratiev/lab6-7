package com.petProject.Accounting.controllers;

import com.petProject.Accounting.entities.Budget;
import com.petProject.Accounting.entities.Transaction;
import com.petProject.Accounting.repositories.TransactionRepository;
import com.petProject.Accounting.service.BudgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.math.BigDecimal;
import java.util.*;

@Controller
public class AnalyticsController {
    @Autowired
    private BudgetService budgetService;
    @Autowired
    private TransactionRepository transactionRepository;
    @GetMapping("/analytics")
    public String showAnalyticsPage(Model model) {
        List<Budget> budgets = budgetService.getAllBudgets();
        Map<String, List<Transaction>> budgetTransactions = new HashMap<>();
        List<Transaction> transactions;
        ArrayList<BigDecimal> sumTransactions = new ArrayList<>();
        ArrayList<Integer> countTransactions = new ArrayList<>();
        int countOfTransactions = 0;
        for (Budget budget : budgets) {
            transactions = transactionRepository.findByBudget(budget);
            budgetTransactions.put(budget.getName(), transactions);
            BigDecimal summ = BigDecimal.ZERO;
            for (int i = 0; i < transactions.size(); i++) {
                summ = summ.add(transactions.get(i).getAmount());
                countOfTransactions += 1;
            }
            sumTransactions.add(summ);
            countTransactions.add(countOfTransactions);
            countOfTransactions = 0;
        }
        ArrayList<String> listOfKeys = new ArrayList<>(budgetTransactions.keySet());
        String[] listOfKeysArray = listOfKeys.toArray(new String[0]);
        model.addAttribute("budgetTransactions", budgetTransactions);
        model.addAttribute("listOfKeysArray", listOfKeysArray);
        return "analytics";
    }
}