## Лабораторна робота 6-7

### Тема: "Spring"

#### Виконав: Кондратьєв Олександр

![Скріншот1](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/1.png?ref_type=heads)

**структура проєкту**
---
![Скріншот2](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/2.png?ref_type=heads)
**MainController**
---
![Скріншот3](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/3.png?ref_type=heads)
![Скріншот4](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/4.png?ref_type=heads)
**BudgetController**
---
![Скріншот5](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/5.png?ref_type=heads)
![Скріншот6](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/6.png?ref_type=heads)
![Скріншот7](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/7.png?ref_type=heads)
**TransactionController**
---
![Скріншот8](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/8.png?ref_type=heads)
**AnalyticsController**
---
![Скріншот9](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/9.png?ref_type=heads)
**Budget entity**
---
![Скріншот10](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/10.png?ref_type=heads)
**Transaction entity**
---
![Скріншот11](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/11.png?ref_type=heads)
**BudgetRepository**
---
![Скріншот12](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/12.png?ref_type=heads)
**TransactionRepository**
---
![Скріншот13](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/13.png?ref_type=heads)
**BudgetService**
---
![Скріншот14](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/14.png?ref_type=heads)
**TransactionService**
---
![Скріншот15](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/15.png?ref_type=heads)
**вітальна сторінка**
---
![Скріншот16](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/16.png?ref_type=heads)
**сторінка з бюджетами**
---
![Скріншот17](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/17.png?ref_type=heads)
**створений бюджет**
---
![Скріншот18](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/18.png?ref_type=heads)
**редагування бюджету**
---
![Скріншот19](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/19.png?ref_type=heads)
**сторінка з додаванням транзакцій**
---
![Скріншот20](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/20.png?ref_type=heads)
**додавання транзакції**
---
![Скріншот21](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/22.png?ref_type=heads)
**редагування транзакції**
---
![Скріншот22](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/23.png?ref_type=heads)
**оновлена транзакція**
---
![Скріншот23](https://gitlab.com/oleksandrkondratiev/lab6-7/-/raw/main/images/24.png?ref_type=heads)
**сторінка з аналітикою**
---